import {shallowMount, createLocalVue} from "@vue/test-utils";
import expect from 'expect';
import Vuex from 'vuex';
import AddRelationshipComponent from "../../../resources/js/components/UMLBuilder/AddRelationshipComponent";

const  localVue = createLocalVue();

localVue.use(Vuex);

let store = new Vuex.Store({
    modules: {
        UMLBuilder: {
            namespaced: true,
            state: {
                selectedModel: {
                    id: 1,
                    name: 'TestModel'
                }
            },
            models: []
        }
    },
    state: {
        settings: {

        }
    }
    })
;

describe('makeRelationshipName', () => {
   let wrapper = shallowMount(AddRelationshipComponent, {store, localVue});

   it('should creates a plural name', () => {
       let result = wrapper.vm.makeRelationshipName('Test', true)
       expect(result).toBe('tests');
   })

    it('should creates a singular name', () => {
        let result = wrapper.vm.makeRelationshipName('Test', false)
        expect(result).toBe('test');
    })
});

describe('makeForeignKeyName', () => {
    let wrapper = shallowMount(AddRelationshipComponent, {store, localVue});

    it('should return a formatted key name', function () {
        let result = wrapper.vm.makeForeignKeyName('testModel');
        expect(result).toBe('test_model_id')
    });
})
