import {mount, createLocalVue} from "@vue/test-utils";
import {BModal, BButton, BFormGroup, BFormInput, BButtonGroup, BPopover} from 'bootstrap-vue';
import expect from 'expect';
import Vuex from 'vuex';
import ModelComponent from "../../../resources/js/components/UMLBuilder/ModelComponent";

const  localVue = createLocalVue();

localVue.use(Vuex);

let store = new Vuex.Store({
        modules: {
            UMLBuilder: {
                namespaced: true,
                state: {
                },
                models: []
            }
        },
        state: {
            settings: {

            }
        }
    })

let stubs = {
    "b-modal": BModal,
    "b-button": BButton,
    "b-button-group": BButtonGroup,
    "b-popover": BPopover,
    "b-form-group": BFormGroup,
    "b-form-input": BFormInput,
}

let propsData = {
    model: {
        name: 'TestModel',
        children: [],
        parents: [],
        fields: [],
        attributes: {
            resource: {
                path: 'test',
                name: 'Test'
            }
        }

    }
}

let wrapper = mount(ModelComponent, {store, localVue, stubs, propsData});

describe('hasResource', () => {

    it("it returns true when a resource exists", () => {
        expect(wrapper.vm.hasResource).toBeTruthy();
    })

    it("it returns false when no resource exists", () => {
        propsData.model.attributes = {};
        let wrapper = mount(ModelComponent, {store, localVue, stubs, propsData});
        expect(wrapper.vm.hasResource).toBeFalsy();
    })

    it("it returns false on a null attribute value", () => {
        propsData.model.attributes = null;
        let wrapper = mount(ModelComponent, {store, localVue, stubs, propsData});
        expect(wrapper.vm.hasResource).toBeFalsy();
    })

})

