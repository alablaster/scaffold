import {mount, createLocalVue} from "@vue/test-utils";
import {BModal, BButton, BFormGroup, BFormInput, BButtonGroup, BPopover} from 'bootstrap-vue';
import expect from 'expect';
import Vuex from 'vuex';
import AddModelComponent from "../../../resources/js/components/UMLBuilder/ControlPanel/AddModelComponent";

const  localVue = createLocalVue();

localVue.use(Vuex);

let store = new Vuex.Store({
        modules: {
            UMLBuilder: {
                namespaced: true,
                state: {
                    models: [
                        {
                            id: 1,
                            name: 'TestName'
                        },
                        {
                            id: 2,
                            name: 'SecondName'
                        }
                    ]
                },

            }
        }
    })

let stubs = {
    "b-modal": BModal,
    "b-button": BButton,
    "b-button-group": BButtonGroup,
    "b-popover": BPopover,
    "b-form-group": BFormGroup,
    "b-form-input": BFormInput,
}

let wrapper = mount(AddModelComponent, {store, localVue, stubs});

describe('nameExists', () => {
    it('Returns true when a another model has the same name', () => {
       expect(wrapper.vm.nameExists('TestName')).toBeTruthy();
    });

    it('Returns false when another model does not have the same name', () => {
        expect(wrapper.vm.nameExists('NotATestName')).toBeFalsy();
    })

})

