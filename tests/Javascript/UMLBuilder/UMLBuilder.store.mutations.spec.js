import expect from 'expect';
import mutations from "../../../resources/js/components/UMLBuilder/mutations";

describe('ADD_MODEL', () => {
    it("Adds a new model the model array", () => {
        const model = {
            name: "New Model"
        }

        const state = {
            models: [
                {
                    name: "Old Model"
                }
            ]
        }

        mutations.ADD_MODEL(state, model);

        expect(state).toEqual({
            models: [
                {
                    name: "Old Model"
                },
                {
                    name: "New Model"
                }
            ]
        })
    })

} )

describe('ADD_NEW_FIELD', () => {
    it("Adds a new field to the correct model in the models array", () => {

        let field = {
            scaffold_model_id: 2,
            name: "test_name"
        }
        let state = {
            models: [
                {
                    id: 1,
                    fields: []
                },
                {
                    id: 2,
                    fields: [
                        {
                            scaffold_model_id: 2,
                            name: "first_name"
                        }
                    ]
                }
            ]
        }

        mutations.ADD_NEW_FIELD(state, field);

        expect(state).toEqual({
            models: [
                {
                    id: 1,
                    fields: []
                },
                {
                    id: 2,
                    fields: [
                        {
                            scaffold_model_id: 2,
                            name: "first_name"
                        },
                        {
                            scaffold_model_id: 2,
                            name: "test_name"
                        }
                    ]
                }
            ]

        })
    });
    it("Adds the first field to the correct model in the models array", () => {

        let field = {
            scaffold_model_id: 1,
            name: "test_name"
        }
        let state = {
            models: [
                {
                    id: 1,
                },
                {
                    id: 2,
                    fields: [
                        {
                            scaffold_model_id: 2,
                            name: "first_name"
                        }
                    ]
                }
            ]
        }

        mutations.ADD_NEW_FIELD(state, field);

        expect(state).toEqual({
            models: [
                {
                    id: 1,
                    fields: [{
                        scaffold_model_id: 1,
                        name: "test_name"
                    }]
                },
                {
                    id: 2,
                    fields: [
                        {
                            scaffold_model_id: 2,
                            name: "first_name"
                        }
                    ]
                }
            ]

        })
    });


    describe('UPDATE_MODEL', () => {
        it("Updates info on existing model", () => {

            const model = {
                id: 2,
                name: "newName",
                attributes: {
                    'resource': {
                        name: 'Test',
                        source: 'test'
                    }
                },
                fields: []
            }
            const state = {
                models: [
                    {
                        id: 1,
                        fields: []
                    },
                    {
                        id: 2,
                        name: "name",
                        fields: []
                    }
                ]

            }

            mutations.UPDATE_MODEL(state, model);

            expect(state).toEqual({
                models: [
                    {
                        id: 1,
                        fields: []
                    },
                    {
                        id: 2,
                        name: "newName",
                        attributes: {
                            'resource': {
                                name: 'Test',
                                source: 'test'
                            }
                        },
                        fields: []
                    }
                ]

            })
        });
    });
});
