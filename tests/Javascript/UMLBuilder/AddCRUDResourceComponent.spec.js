import {mount, createLocalVue} from "@vue/test-utils";
import { BModal, BButton, BFormGroup, BFormInput } from 'bootstrap-vue';
import expect from 'expect';
import Vuex from 'vuex';
import AddCRUDResourceComponent from "../../../resources/js/components/UMLBuilder/AddCRUDResourceComponent";
import mutations from "../../../resources/js/components/UMLBuilder/mutations";

const  localVue = createLocalVue();

localVue.use(Vuex);

let store = new Vuex.Store({
        modules: {
            UMLBuilder: {
                namespaced: true,
                state: {
                    selectedModel: {
                        id: 1,
                        name: 'TestModel'
                    }
                },
                models: []
            }
        },
        state: {
            settings: {

            }
        }
    })

let stubs = {
    "b-modal": BModal,
    "b-button": BButton,
    "b-form-group": BFormGroup,
    "b-form-input": BFormInput
}

const wrapper = mount(AddCRUDResourceComponent, {store, localVue, stubs});


describe('urlSafe', () => {
    it("it returns a lowercase version of the route", () => {
        expect(wrapper.vm.urlSafe('Test')).toBe('test');

    })


})

describe('urlSafe', () => {
    it("it returns a hyphenated version of CamelCase", () => {
        expect(wrapper.vm.urlSafe('TestRoute')).toBe('test-route');
    })

})
