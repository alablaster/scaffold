<?php

namespace Tests\Feature;

use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Projects\Entities\Project;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic feature test example.
     * @test
     * @return void
     */
    public function deletes_model()
    {
        $project = Project::create(['name' => 'test Project']);
        $model = ['name' => 'TestModel', 'project_id' => $project->id];
        $newModel = ScaffoldModel::create($model);
        $this->assertDatabaseHas('scaffold_models', $model);
        $response = $this->delete(route('models.destroy', [$project, $newModel]));
        $this->assertDatabaseMissing('scaffold_models', $model);
        $response->assertStatus(204);
    }
}
