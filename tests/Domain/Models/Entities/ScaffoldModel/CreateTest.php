<?php

namespace Tests\Feature;

use App\Domain\Projects\Entities\Project;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\Assert;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function displayForm()
    {
        $project = Project::create(['name' => 'TestProject']);
        $response = $this->get(route('models.create', $project->id));
        $response->assertSee('Name');
    }
}
