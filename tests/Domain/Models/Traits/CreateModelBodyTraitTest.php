<?php

namespace Tests\Domain\Models\Traits;

use App\Domain\Models\Entities\Field;
use App\Domain\Models\Traits\CreateModelBodyTrait;
use PHPUnit\Framework\TestCase;
use Webmozart\Assert\Assert;

class CreateModelBodyTraitTest extends TestCase
{

    use CreateModelBodyTrait;

    /**
     * @test
     */
    public function write_fillable_array_returns_mass_assignable_field()
    {
        $field = (object) [
            'name' => 'test_field',
            'label' => 'Test Field',
            'attributes' => ['Mass Assignable']
        ];

        $fields = [
            $field
        ];

        $actual = $this->writeFillableArray($fields);

        $this->assertStringContainsString($field->name, $actual);
    }

}
