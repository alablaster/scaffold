<?php

namespace Tests\Domain\Projects\Traits;

use App\Domain\Projects\Traits\DirectoryBrowserTrait;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class DirectoryBrowserTraitTest extends TestCase
{
    use DirectoryBrowserTrait;

    	public function testAddLayer()
    {
        $result = $this->addLayer( "second", 'reference["first"]');

        Assert::assertSame('reference["first"]["second"]', $result);
    }

    public function testTrimDirectory()
    {
        $file = 'this/is/a/test';
        $directory = '/this/is/';
        $result = $this->trimDirectory($file, $directory);

        Assert::assertSame('a/test', $result);
    }

    public function testAddFileToArray()
    {
        $file = 'test/another/path/to/something.php';
        $return = [
            'test' => [
                'path' => [
                    'to' => [
                        'file.php' => 'test/path/to/file.php'
                    ]
                ]
            ]
        ];
        $directory = 'test';
        $expected = [
            'test' => [
                'path' => [
                    'to' => [
                        'file.php' => 'test/path/to/file.php'
                        ]
                    ],
                ],
                'another' => [
                    'path' => [
                        'to' => [
                            'something.php' => 'test/another/path/to/something.php'

                    ]
                ]
            ]
        ];

        $result = $this->addFileToArray($file, $return, $directory);

        Assert::assertSame($expected, $result);
    }
}
