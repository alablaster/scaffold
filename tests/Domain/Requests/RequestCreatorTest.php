<?php

namespace Tests\Domain\Requests;

use App\Domain\Models\Entities\Field;
use App\Domain\Requests\RequestCreator;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class RequestCreatorTest extends TestCase
{

    public function invokeMethod($methodName, array $parameters = array())
    {
        $object = new RequestCreator();
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }

    /**
     * @test
     */
    public function field_type_returns_with_max255_for_strings()
    {
        $result = $this->invokeMethod('fieldType', ['string'] );

        Assert::assertStringContainsString('max:255', $result);
    }

    /**
     * @test
     */
    public function field_type_doesnt_return_with_max255_for_integers()
    {
        $result = $this->invokeMethod('fieldType', ['integer'] );

        Assert::assertStringNotContainsString('max:255', $result);
    }

    /**
     * @test
     */
    public function nullableRule_returns_requires_for_nullable_store()
    {
        $result = $this->invokeMethod( 'nullableRule', [false, 'Store'] );

        Assert::assertStringContainsString('required', $result);
    }

    /**
     * @test
     */
    public function nullableRule_returns_sometimes_for_nullable_update()
    {
        $result = $this->invokeMethod( 'nullableRule', [false, 'Update'] );

        Assert::assertStringContainsString('sometimes', $result);
    }

    /**
     * @test
     */
    public function nullableRule_returns_null_for_others()
    {
        $result = $this->invokeMethod('nullableRule', [true, 'Update'] );

        Assert::assertNull($result);
    }

}
