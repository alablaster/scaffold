<?php

namespace Tests\Browser;

use App\Domain\Core\Entities\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{

    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testsLoginRedirectsToProjects()
    {
        $user = User::create([
            'name' => 'Test User',
            'email' => 'user@example.com',
            'password' => bcrypt('secret'),
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Scaffold')
                    ->type('email', 'user@example.com')
                    ->type('password', 'secret')
                    ->press('Login')
                    ->waitForLocation('/projects');
        });
    }
}
