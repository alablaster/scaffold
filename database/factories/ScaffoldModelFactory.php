<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Domain\Models\Entities\ScaffoldModel::class, function (Faker $faker) {
    return [
        'name' => \Illuminate\Support\Str::Title($faker->word)
    ];
});
