<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Domain\Models\Entities\Field::class, function (Faker $faker) {

    $name = $faker->words(3, true);

    return [
        'label' => \Illuminate\Support\Str::title($name),
        'name' => \Illuminate\Support\Str::snake($name),
        'type' => 'string'
    ];
});
