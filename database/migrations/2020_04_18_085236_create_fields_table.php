<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('scaffold_model_id')->unsigned();
            $table->string('label');
            $table->string('name');
            $table->string('type');
            $table->json('attributes')->default(json_encode(array()));
            $table->timestamps();

            $table->foreign('scaffold_model_id')
                ->references('id')
                ->on('scaffold_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
