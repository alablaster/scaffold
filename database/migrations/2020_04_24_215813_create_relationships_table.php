<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationships', function (Blueprint $table) {
            $table->id();
            $table->string('relationship_type');
            $table->bigInteger('parent_model_id')->unsigned();
            $table->string('parent_relationship');
            $table->string('parent_foreign_key');
            $table->bigInteger('child_model_id')->unsigned();
            $table->string('child_relationship');
            $table->string('child_foreign_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationships');
    }
}
