<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = factory(\App\Domain\Projects\Entities\Project::class, 5)->create();

        foreach($projects as $project) {
            $models = factory(\App\Domain\Models\Entities\ScaffoldModel::class, 5)->create(
                [
                    'project_id' => $project->id
                ]
            );

            foreach($models as $model) {
                factory(\App\Domain\Models\Entities\Field::class, 3)->create(
                    ["scaffold_model_id" => $model->id]);
            }
        }
    }
}
