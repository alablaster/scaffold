@extends('master.main')

@section('content')
    <div class="row">
        <div class="col-lg-2">
            <control-panel></control-panel>
        </div>
        <div class="col-10">
            <main role="main" >
                <umlbuilder
                    :initial-models="{{json_encode($models)}}"
                    :settings="{{json_encode(config('scaffold'))}}"
                    :project_id="{{$project_id}}"
                ></umlbuilder>
            </main>
        </div>
    </div>

@endsection
