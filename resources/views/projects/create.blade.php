@extends('master.main')

@section('content')

    <form class="form" action="{{ route('projects.store') }}" method="post">

        @csrf

        <b-form-group
            label="Project Name"
        >
            <input class="form-control" name="name" required>

        </b-form-group>

        <input type="submit" class="btn btn-primary">

    </form>

@endsection
