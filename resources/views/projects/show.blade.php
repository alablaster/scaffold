@extends('master.main')

@section('content')
{{--    <link rel="stylesheet" type="text/css" href="/prettify/prettify.css">--}}

    <h2>
        Project Overview: {{ $project->name }}
    </h2>

    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <generate-project-button :project_id="{{$project->id}}"></generate-project-button>
                    <br>
                    <b-button
                        variant="primary"
                        href="{{ route('umlbuilder', $project->id) }}"
                        class="btn-block"
                    >
                        <i class="fa fa-network-wired"></i>
                        UML Builder
                    </b-button>
                </div>
            </div>
        </div>
        <div class="col-9">
            <directory-viewer-component
                :files="{{json_encode($files)}}"
            />
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
@endpush

