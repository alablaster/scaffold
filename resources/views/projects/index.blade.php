@extends('master.main')

@section('content')

    <div class="col-md-6">
        <div class="card">
            <b-card-header>
                Projects

                <b-button
                    class="btn-sm float-right"
                    variant="primary"
                    href="{{ route('projects.create') }}"
                >
                    Add Project
                </b-button>
            </b-card-header>
            <b-card-body>
                @if($projects->count() > 0)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Project Name</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{ $project->name }}</td>
                            <td align="right">
                                <b-button
                                    variant="primary"
                                    href="{{ route('projects.show', $project->id) }}"
                                >
                                    Project Overview
                                </b-button>
                            </td>
                        </tr>
                    @endforeach
                </table>
                @else
                    <div class="alert alert-info">

                        <strong>
                            No projects created.
                        </strong>
                        <p>
                            No projects have been created. Add one and it will be listed here.
                        </p>

                    </div>
                @endif

            </b-card-body>
        </div>
    </div>

@endsection
