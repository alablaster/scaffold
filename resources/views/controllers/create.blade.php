@extends('master.main')

@section('content')

    <div class="col-md-6">
        <h2>Create Controller</h2>

        <create-controller
            :settings="{{json_encode(config('scaffold'))}}"
            :models="{{json_encode($models)}}"
        />

    </div>

@endsection
