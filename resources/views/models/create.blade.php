@extends('master.main')

@section('title', 'Create Model')

@section('content')

<div class="row">
    <div class="col-md-6">
        <label>Model Name</label>
        <input type="text" class="form-control" name="name" required>

        <Fields
            :field-types="{{json_encode($fieldTypes)}}"
        />
    </div>
</div>

<div class="row">
    <div class="col-12">

    </div>
</div>

@endsection
