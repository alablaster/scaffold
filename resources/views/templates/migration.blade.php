use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('{{$table_name}}', function (Blueprint $table) {
            $table->id();
            @foreach($fields as $field)
                <?php
                    $line = '$table->' . $field->type . "('" . $field->name . "')'";
                    if($field->nullable) $line .= "->nullable()";
                    $line .= ';';
                ?>
                {{$line}}
            @endforeach
            $table->timestamps();

            $table->foreign('scaffold_model_id')
                ->references('id')
                ->on('scaffold_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('{{ $table_name }}');
    }
}
