<header>
    <div>
        <b-navbar toggleable="lg" type="dark" variant="dark">
            <b-navbar-brand href="#">Scaffold App</b-navbar-brand>

            <b-navbar-toggle target="nav-collapse"></b-navbar-toggle>

            <b-collapse id="nav-collapse" is-nav>
                <b-navbar-nav>
                    <b-nav-item href="{{route('projects.create')}}">New Project</b-nav-item>

                    <b-nav-item-dropdown text="Projects" left>
                        @foreach(\App\Domain\Projects\Entities\Project::all() as $project)
                            <b-dropdown-item href="{{route('projects.show', $project->id)}}">{{$project->name}}</b-dropdown-item>
                        @endforeach
                    </b-nav-item-dropdown>
                </b-navbar-nav>

                <!-- Right aligned nav items -->
                <b-navbar-nav class="ml-auto">
                    <b-nav-item-dropdown right>
                        <!-- Using 'button-content' slot -->
                        <template v-slot:button-content>
                            <em>{{ \Illuminate\Support\Facades\Auth::user()->name }}</em>
                        </template>
                        <b-dropdown-item href="{{ route('logout') }}">Sign Out</b-dropdown-item>
                    </b-nav-item-dropdown>
                </b-navbar-nav>
            </b-collapse>
        </b-navbar>
    </div>
</header>
