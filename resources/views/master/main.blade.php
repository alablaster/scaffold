<!doctype html>
<html lang="en" class="h-100">
@include('master._head')

<body class="d-flex flex-column h-100">
<div id="app">

@include('master._header')

<!-- Begin page content -->
    <main role="main" class="flex-shrink-0">
        <div class="container-fluid pt-4">

            @yield('content')

        </div>
    </main>

</div>
@routes
<script src="{{ mix('/js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
