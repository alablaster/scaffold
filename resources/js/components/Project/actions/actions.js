
export default {
    loadFile(context, path)
    {
        axios.post(route('files.show'), {path: path})
            .then((response) => {
                context.commit('SET_CURRENT_FILE', response.data);
                PR.prettyPrint();
            }).catch((error) => {
                this.$bvToast.toast(error.message, {
                    variant: 'warning',
                    title: 'Uh oh!',
                    autoHideDelay: 4000
                })
        })
    }
}
