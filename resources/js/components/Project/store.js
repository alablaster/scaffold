import state from "./state";
import actions from "./actions/actions";
import mutations from "./mutations";

export default {
    namespaced: true,
    state: state,
    actions: actions,
    mutations: mutations,
}
