export default {
    SET_CURRENT_FILE(state, file) {
        state.currentFile = file;
    },

    SET_FILE_LIST(state, list) {
        state.fileList = list;
    }
}
