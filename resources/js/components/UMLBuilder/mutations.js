export default {
    SET_MODELS(state, value) {
        state.models = value;
    },
    SET_SELECTED_MODEL(state, value) {
        state.selectedModel = value;
    },

    // Tested
    ADD_MODEL(state, value) {
        state.models.push(value);
    },

    // Tested
    ADD_NEW_FIELD(state, field){
        state.models.forEach((element, index) => {
            if(element.id === field.scaffold_model_id) {
                console.log(index);
                if(!state.models[index].hasOwnProperty('fields')){
                    state.models[index].fields = [];
                }
                console.log(state.models[index].fields);
                state.models[index].fields.push(field);
            }
        });
    },
    UPDATE_MODEL(state, model){
        state.models.forEach((element, index) => {
            if(element.id == model.id) {
                state.models[index] = model;
            }
        });
    },
    ADD_RELATIONSHIP(state, relationship){
        state.models.forEach((element, index) => {
            if(element.id === relationship.parent_model_id) {
                state.models[index].children.push(relationship)
            }
            if(element.id === relationship.child_model_id) {
                state.models[index].parents.push(relationship)
            }
        });
    }
}
