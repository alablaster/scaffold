export default function (context, id) {
        let models = context.state.models.filter( model => model.id !== id );

        context.commit('SET_MODELS', models);
}
