/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');


window.Vue = require('vue');
import Vuex from 'vuex'

Vue.use(Vuex);

// Import lodash
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
Vue.use(VueLodash, {lodash: lodash })

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('umlbuilder', require('./components/UMLBuilder/UMLBuilder.vue').default);
Vue.component('ControlPanel', require('./components/UMLBuilder/ControlPanel/ControlPanelComponent.vue').default);
Vue.component('CreateController', require('./components/Controller/Create/CreateControllerComponent').default);
Vue.component('GenerateProjectButton', require('./components/Project/GenerateProjectButton').default);
Vue.component('DirectoryViewerComponent', require('./components/Project/DirectoryViewerComponent').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import UMLBuilder from './components/UMLBuilder/store';
import Project from './components/Project/store';
import methods from "./methods/methods";
import setters from "./setters";
const store = new Vuex.Store({
    modules: {
        UMLBuilder: UMLBuilder,
        Project: Project
    },
    mutations: setters
   })

const app = new Vue({
    store,
    el: '#app',
    methods: methods
});
