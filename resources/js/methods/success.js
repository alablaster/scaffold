export default function (context, message) {

    this.$bvToast.toast(message, {
        title: 'That worked!',
        autoHideDelay: 4000,
        variant: 'success'
    })
}
