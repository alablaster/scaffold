export default function (context, error) {
    this.$bvToast.toast(error, {
            title: 'Uh oh!',
            autoHideDelay: 4000,
            variant: 'warning'
    })
}
