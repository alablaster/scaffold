<?php

return [
    "field_types" => [
        "datetime" => [
            "name" => "Date Time",
            "slug" => "datetime",
            "options" => [
                "Nullable" => "nullable"
            ]
        ],
        "string" => [
            "name" => "String",
            "slug" => "string",
            "options" => [
                "Nullable" => "nullable"
            ]
        ],
        "integer" => [
            "name" => "Integer",
            "slug" => "integer",
            "options" => [
                "Nullable" => "nullable"
            ]
        ],
        "text" => [
            "name" => "Text",
            "slug" => "text",
            "options" => [
                "Nullable" => "nullable"
            ]
        ]
    ],
    "base_path" => [
        "migrations" => "database/migrations/",
        "models" => "app/Entities/",
        "requests" => "app/Http/Requests",
        "controllers" => "app/Http/Controllers"
    ],
    "relationshipTypes" => [
        'oneToOne' => [
            'type' => 'oneToOne',
            'parentSlug' => 'hasOne',
            'childSlug' => 'belongsTo',
            'name' => 'One to One'
        ],
        'oneToMany' => [
            'type' => 'oneToMany',
            'parentSlug' => 'hasMany',
            'childSlug' => 'belongsTo',
            'name' => 'One to Many',
        ],
        'manyToMany' => [
            'type' => 'manyToMany',
            'parentSlug' => 'hasMany',
            'childSlug' => 'belongsTo',
            'name' => 'Many to Many',
        ],
    ],
    "methodActions" => [
        'GET',
        'POST',
        'PUT',
        'PATCH',
        'DELETE'
    ]
];
