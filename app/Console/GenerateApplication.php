<?php

namespace App\Console\Commands;

use App\Contracts\ScaffoldsApplication;
use App\Domain\Controllers\Traits\CreateResourceTrait;
use App\Domain\Core\Traits\CreateFilesTrait;
use App\Domain\Migrations\Traits\CreateDatabaseTrait;
use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Models\Traits\CreateModelsTrait;
use App\Domain\Projects\Entities\Project;
use App\Domain\Requests\Traits\CreateRequestTrait;
use App\Traits\ScaffoldLaravelTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class GenerateApplication extends Command implements ScaffoldsApplication
{
    use CreateDatabaseTrait;
    use CreateModelsTrait;
    use CreateResourceTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scaffold:laravel {project_id} {--silent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $models;

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $this->models = Project::find($this->argument('project_id'))->models;

        $base_path = ('staging/project_' . $this->argument('project_id')) . '/';

        File::deleteDirectory(base_path('storage/app/' . $base_path));
        File::copyDirectory(base_path('storage/app/template_apps/laravel_7_0'), base_path('storage/app/' . $base_path));
        $this->createDatabase($this->models, $base_path, $this->option('silent'));
        $this->createModels($this->models, $base_path, $this->option('silent'));
        $this->createResources($this->models, $base_path, $this->option('silent'));
    }

}
