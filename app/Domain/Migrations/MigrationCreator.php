<?php


namespace App\Domain\Migrations;

use App\Domain\Core\Traits\CreateFilesTrait;
use App\Domain\Core\Traits\PivotTablesTrait;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class MigrationCreator
{
    use CreateFilesTrait;
    use PivotTablesTrait;

    private $migrationCount = 1;

    public function create($name, $fields, $base_path)
    {

        // First we will get the stub file for the migration, which serves as a type
        // of template for the migration. Once we have those we will populate the
        // various place-holders, save the file, and run the post create event.
        $stub = $this->getStub();

        $path = $this->migrationPath($name, $base_path);


        $this->storeFile(
            $path,
            $this->populateStub($stub, $name, $this->formatFields($fields))
        );

        return $path;
    }

    public function createPivotTable($class_a, $class_b, $base_path)
    {

        // First we will get the stub file for the migration, which serves as a type
        // of template for the migration. Once we have those we will populate the
        // various place-holders, save the file, and run the post create event.
        $stub = $this->getStub('pivot');

        $name = $this->createPivotTableName($class_a, $class_b);
        $path = $this->migrationPath($name, $base_path);


        $this->storeFile(
            $path,
            $this->populatePivotStub($stub, $name, $class_a, $class_b)
        );

        return $path;
    }

    /**
     * @param $stub
     * @param $name
     * @param $fields
     * @return string|string[]
     */
    protected function populateStub($stub, $name, $fields){
        $stub = str_replace(
            ['{{ class }}'],
            $this->getClassName($name), $stub
        );

        $stub = str_replace(
            ['{{ table }}'],
            Str::lower(Str::plural($name)), $stub
        );

        $stub = str_replace(
            ['{{ fields }}'],
            $fields, $stub
        );

        return $stub;
    }

    protected function populatePivotStub($stub, $name, $class_a, $class_b){
        $stub = str_replace(
            ['{{ class }}'],
            $this->getClassName($name), $stub
        );

        $stub = str_replace(
            ['{{ table }}'],
            $name, $stub
        );

        $stub = str_replace(
            ['{{ field_a }}'],
            Str::snake($class_a) . '_id', $stub
        );

        $stub = str_replace(
            ['{{ field_b }}'],
            Str::snake($class_b) . '_id', $stub
        );



        return $stub;
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub($stub = 'default') {

        switch ($stub) {
            case 'default':
                return $this->files->get(__DIR__ . '/stubs/migration.create.stub');
            case 'pivot':
                return $this->files->get(__DIR__ . '/stubs/migration.pivot.stub');

        }
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::pluralStudly($name);
    }

    private function formatFields($fields) {

        $return = '';
        foreach($fields as $field) {
            $line = "\t\t\t";
            $line .= '$table->' . $field['type'] . "('" . $field['name'] . "')";
            if(in_array('Nullable', $field['attributes'])) $line .= "->nullable()";
            if(in_array('Unsigned', $field['attributes'])) $line.= "->unsigned()";
            $line .= ';' . "\n";

            $return .= $line;
        }

        return $return;
    }

    private function migrationPath($name, $path) {

        $path .= '/database/migrations/';
        $path .= date('Y_m_d') . '_';
        $path .= sprintf('%06d', $this->migrationCount);
        $this->migrationCount++;
        $path .= '_create_';
        $path .= $this->tableName($name);
        $path .= '_table.php';
        return $path;
    }

    private function tableName($name)
    {
        return Str::of(Str::snake($name))->plural();
    }

}
