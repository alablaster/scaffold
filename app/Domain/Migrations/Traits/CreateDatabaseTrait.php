<?php

namespace App\Domain\Migrations\Traits;

use App\Domain\Migrations\MigrationCreator;
use App\Domain\Models\Entities\Relationship;
use App\Domain\Models\Entities\ScaffoldModel;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

trait CreateDatabaseTrait
{
    private $migrationCount = 1;

    /**
     * @param $models
     */
    public function createDatabase($models, $base_path, $quiet){

        $creator = new MigrationCreator();

        foreach ($models as $model)
        {
            $name = $model->name;
            $fields = $model->fields;
            $creator->create($name, $fields, $base_path);
            if(!$quiet) print("\n" . 'Migration for ' . $name . ' created');
        }

        foreach(Relationship::where('relationship_type', 'manyToMany')->get() as $relationship) {
            $creator->createPivotTable($relationship->parent->name, $relationship->child->name, $base_path);
            if(!$quiet) print("\n" . 'Migration for ' . $name . ' created');
        }
    }


}
