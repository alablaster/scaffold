<?php


namespace App\Domain\Requests\Traits;


use App\Domain\Requests\RequestCreator;

trait CreateRequestTrait
{
    public function createRequest($models) {

        $creator = new RequestCreator;

        foreach($models as $model) {
            $creator->create($model);

            print("\n" . 'Request for ' . $model->name . ' created' );
        }
    }
}
