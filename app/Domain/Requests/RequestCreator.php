<?php


namespace App\Domain\Requests;


use App\Domain\Core\FileCreator;
use App\Domain\Core\Traits\CreateFilesTrait;
use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Models\Traits\CreateModelBodyTrait;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RequestCreator extends FileCreator
{
    use CreateModelBodyTrait;
    use CreateFilesTrait;

    /**
     * @param ScaffoldModel $model
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function create(ScaffoldModel $model, $class, $base_path)
    {

        // First we will get the stub file for the request, which serves as a type
        // of template for the request. Once we have those we will populate the
        // various place-holders, save the file, and run the post create event.

        $stub = $this->getStub();

        $path = $this->getPath('request', $model, Str::snake($class), $base_path);

        $namespace = $this->getNamespace($model);

        $rules = $this->writeRules($model->fields, $class);

        $this->storeFile(
            $path,
            $this->populateStub($stub, $namespace, $class, $rules)
        );

        return $path;
    }

    /**
     * @param $stub
     * @param $namespace
     * @param $class
     * @param $rules
     * @return string|string[]
     */
    protected function populateStub($stub, $namespace, $class, $rules){

        $stub = str_replace(
            ['{{ namespace }}'],
            $namespace, $stub
        );

        $stub = str_replace(
            ['{{ class }}'],
            $class, $stub
        );

        $stub = str_replace(
            ['{{ rules }}'],
            $rules, $stub
        );

        return $stub;
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub() {
        return $this->files->get(__DIR__ . '/stubs/request.stub');
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name) . 'Request';
    }

    private function getNamespace($model) {
        return "\\" . $model->name;
    }

    private function writeRules($fields, $class) {
        $rules = '';
        $ignored = [
            'id', 'created_at', 'updated_at', 'deleted_at'
        ];
        foreach($fields as $field) {
            if(!in_array($field->name, $ignored)) {
                $rules .= "\n\t\t\t" . "'" . $field->name . "' => [";
                $rules .= $this->nullableRule(isset($field->attributes['Nullable']), $class);
                $rules .= $this->fieldType($field->type);
                $rules .= "\n\t\t\t" . '],';
            }
        }

        return $rules;
    }

    private function nullableRule($nullable, $class){
        if(!$nullable && $class == 'Store') {
            return "\n\t\t\t\t" .  "'required',";
        } else if (!$nullable && $class == 'Update') {
            return "\n\t\t\t\t" .  "'sometimes',";
        } else {
            return null;
        }
    }

    private function fieldType($type) {
        $return = "\n\t\t\t\t'" . $type . "',";
        if($type == 'string') {
            $return .= "\n\t\t\t\t" .  "'max:255',";
        }

        return $return;
    }
}
