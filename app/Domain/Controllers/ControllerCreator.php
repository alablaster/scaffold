<?php


namespace App\Domain\Controllers;

use App\Domain\Core\FileCreator;
use App\Domain\Core\Traits\CreateFilesTrait;
use App\Domain\Core\Traits\PivotTablesTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class ControllerCreator extends FileCreator
{

    use CreateFilesTrait;

    public function create($type, $model, $base_path)
    {

        $data = [];

        // We will direct the create method to the correct type of
        // controller to be created.

        switch ($type) {
            case 'crud':
                $data = $this->loadCrudData($model);
                break;

            case 'evoke':
                // TODO: implement evoke controller
                break;

            case 'rest':
                // TODO: implement rest controller
        }

        // Next we will get the stub file for the controller, which serves as a type
        // of template for the controller. Once we have those we will populate the
        // various place-holders and save the file.
        $stub = $this->getStub($type);

        $path = $this->getPath('controller', $model, $base_path);


        $this->storeFile(
            $path,
            $this->populateStub($stub, $data)
        );

        return $path;
    }

    /**
     * @param $stub
     * @param $name
     * @param $fields
     * @return string|string[]
     */
    protected function populateStub($stub, $data){

        foreach($data as $key => $value) {
            $stub = str_replace(
                ["{{ $key }}"],
                $value, $stub
            );
        }

        return $stub;
    }

    protected function loadCrudData($model){
        return [
            "class" => $model->attributes['resource']['name'] . 'Controller',
            "full_model" => $model->full_name,
            "model" => $model->name,
            "namespace" => '', // TODO: implement namespace
            "route" => $model->attributes['resource']['route'],
            "variable"  => Str::snake($model->name),
            "variables" => Str::plural(Str::snake($model->name), 2),
            "viewDirectory" => Str::snake($model->name), // TODO: update file structure for namespaces
        ];
    }

    protected function getStub($stub)
    {
        switch ($stub) {
            case 'crud':
                return $this->files->get(__DIR__ . '/stubs/crudController.create.stub');
        }
    }

}
