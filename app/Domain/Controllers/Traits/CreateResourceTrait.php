<?php


namespace App\Domain\Controllers\Traits;


use App\Domain\Controllers\ControllerCreator;
use App\Domain\Requests\RequestCreator;

trait CreateResourceTrait
{

    public function createResources($models, $base_path, $quiet) {
        $controller_creator = new ControllerCreator();
        $request_creator = new RequestCreator;

        foreach($models as $model) {
            if(isset($model->attributes['resource'])) {
                $controller_creator->create('crud', $model, $base_path);
                $request_creator->create($model, 'Store', $base_path);
                $request_creator->create($model, 'Update', $base_path);
                if(!$quiet) print("\nResource for $model->name created");
            }
        }
    }
}
