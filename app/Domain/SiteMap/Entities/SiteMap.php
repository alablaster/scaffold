<?php

namespace App\Domain\SiteMap\Entities;

use Illuminate\Database\Eloquent\Model;

class SiteMap extends Model
{
    protected $fillable = [
        'name',
    ];
}
