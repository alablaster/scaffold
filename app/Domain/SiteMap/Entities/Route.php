<?php

namespace App\Domain\SiteMap\Entities;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = [
        'parent_id',
        'index',
        'name',
        'route',
        'method',
        'prams',
        'returns'
    ];
}
