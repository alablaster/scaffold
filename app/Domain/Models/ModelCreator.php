<?php


namespace App\Domain\Models;


use App\Domain\Core\Traits\CreateFilesTrait;
use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Models\Traits\CreateModelBodyTrait;
use Illuminate\Support\Str;

class ModelCreator
{
    use CreateModelBodyTrait;
    use CreateFilesTrait;
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */


    /**
     * @param ScaffoldModel $model
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function create(ScaffoldModel $model, $base_path)
    {

        // First we will get the stub file for the model, which serves as a type
        // of template for the model. Once we have those we will populate the
        // various place-holders, save the file, and run the post create event.

        $stub = $this->getStub();

        $class = $this->getClassName($model->name);

        $path = $this->modelPath($class, $base_path);

        $namespace = $this->getNamespace($model);

        $body = $this->createModelBody($model);

        $this->storeFile(
            $path,
            $this->populateStub($stub, $namespace, $class, $body)
        );

        return $path;
    }

    /**
     * @param $stub
     * @param $name
     * @param $fields
     * @return string|string[]
     */
    protected function populateStub($stub, $namespace, $class, $body){

        $stub = str_replace(
            ['{{ namespace }}'],
            $namespace, $stub
        );

        $stub = str_replace(
            ['{{ class }}'],
            $class, $stub
        );

        $stub = str_replace(
            ['{{ body }}'],
            $body, $stub
        );

        return $stub;
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function getStub() {
        return $this->files->get(__DIR__ . '/stubs/model.stub');
    }

    /**
     * Get the class name of a migration name.
     *
     * @param  string  $name
     * @return string
     */
    protected function getClassName($name)
    {
        return Str::studly($name);
    }

    private function modelPath($class, $base_path) {

        $path =  $base_path . '/app/Entities/';
        $path .= $class;
        $path .= '.php';
        return $path;
    }

    private function getNamespace($model) {
        return 'App\\Entities';
    }
}
