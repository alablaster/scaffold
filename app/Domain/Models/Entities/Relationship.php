<?php

namespace App\Domain\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    protected $fillable = [
        'relationship_type',
        'parent_model_id',
        'parent_relationship',
        'parent_foreign_key',
        'child_model_id',
        'child_relationship',
        'child_foreign_key'
    ];

    public function parent()
    {
        return $this->hasOne(ScaffoldModel::class, 'id', 'parent_model_id');
    }

    public function child()
    {
        return $this->hasOne(ScaffoldModel::class, 'id', 'child_model_id');
    }
}
