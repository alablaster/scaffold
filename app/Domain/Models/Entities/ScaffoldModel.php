<?php

namespace App\Domain\Models\Entities;

use Illuminate\Database\Eloquent\Model;
use function foo\func;

class ScaffoldModel extends Model
{
    protected $fillable = ['name', 'project_id'];

    protected $casts = [
        'attributes' => 'array'
    ];

    public function fields() {
        return $this->hasMany(Field::class);
    }
//
//    public function getFieldsAttribute() {
//        return $this->generatedFields();
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany(Relationship::class, 'parent_model_id');
    }

    public function parents() {
        return $this->hasMany(Relationship::class, 'child_model_id');
    }

    public function getFullNameAttribute() {
        return 'App\\Entities\\' . $this->name;
    }

//
//
//    private function generatedFields() {
//        $generatedFields['id'] = [
//            'label' => 'ID',
//            'name' => 'id',
//            'type' => 'incrementing',
//            'tags' => [
//                'Primary Key'
//            ]
//        ];
//
//        $generatedFields = $this->addForeignKeyFields($generatedFields);
//
//        $generatedFields = $this->addRelationshipFields($generatedFields);
//
//        $generatedFields = $this->addCustomFields($generatedFields);
//
//
//        $generatedFields['created_at'] = [
//            'label' => 'Created At',
//            'name' => 'created_at',
//            'type' => 'datetime',
//            'tags' => []
//        ];
//
//        $generatedFields['updated_at'] = [
//            'label' => 'Updated At',
//            'name' => 'updated_at',
//            'type' => 'datetime',
//            'tags' => []
//        ];
//
//        if($this->softDeletable) {
//            $generatedFields['deleted_at'] = [
//                'label' => 'Deleted At',
//                'name' => 'deleted_at',
//                'type' => 'datetime',
//                'tags' => []
//            ];
//        }
//
//        return $generatedFields;
//    }

//    private function addRelationshipFields(array $generatedFields)
//    {
//        foreach($this->fieldRelationships() as $field)
//        {
//            $generatedFields[$field->name] = [
//                'label' => $field->label,
//                'name' => $field->name,
//                'type' => $field->type,
//            ];
//
//            if($field->nullable){
//                $generatedFields[$field->name]['tags'][] = 'Nullable';
//            }
//
//            if($field->massAssignable){
//                $generatedFields[$field->name]['tags'][] = 'Mass Assignable';
//            }
//        }
//
//        return $generatedFields;
//    }
//
//    private function addForeignKeyFields(array $generatedFields)
//    {
//        foreach($this->parents as $field)
//        {
//            $generatedFields[$field->parent_foreign_key] = [
//                'label' => $field->parent_foreign_key,
//                'name' => $field->parent_foreign_key,
//                'type' => 'bigInteger'
//            ];
//
//            $generatedFields[$field->parent_foreign_key]['tags'][] = 'Unsigned';
//            $generatedFields[$field->parent_foreign_key]['tags'][] = 'Foreign Key';
//        }
//
//        foreach($this->children as $field)
//        {
//            $generatedFields[$field->child_foreign_key] = [
//                'label' => $field->child_foreign_key,
//                'name' => $field->child_foreign_key,
//                'type' => 'bigInteger',
//            ];
//
//            $generatedFields[$field->child_foreign_key]['tags'][] = 'Unsigned';
//            $generatedFields[$field->child_foreign_key]['tags'][] = 'Foreign Key';
//        }
//
//        return $generatedFields;
//    }
//
//    private function addCustomFields($generateFields) {
//        foreach($this->fieldRelationships as $field) {
//            $generateFields[$field->name] = $field;
//        }
//
//        return $generateFields;
//    }
}
