<?php

namespace App\Domain\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'label',
        'name',
        'type',
        'nullable',
        'mass_assignable',
        'scaffold_model_id',
        'attributes'
    ];

    protected $casts = [
        'attributes' => 'array'
    ];
}
