<?php


namespace App\Domain\Models\Traits;


use App\Domain\Core\Traits\PivotTablesTrait;

trait CreateModelBodyTrait
{
    use PivotTablesTrait;

    public function createModelBody($model) {

        $return = '';

        $return .= $this->writeFillableArray($model->fields);
        $return .= $this->writeRelationships($model->children, $model->parents);

        return $return;
    }

    private function writeFillableArray($fields) {

        $return = null;

        $fillable = '';

        foreach($fields as $field) {
            if(in_array('Mass Assignable', $field->attributes)) {
                $fillable .= $this->addFillableField($field->name);
            }
        }

        if($fillable != '') {
            $return = "\t" . 'protected $fillable = [';
            $return .= $fillable;
            $return .= "\n\t];";
        }

        return $return;
    }

    private function addFillableField($name)
    {
        return "\n\t\t'" . $name . "',";
    }

    private function writeRelationships($children, $parents) {
        $return = '';

        foreach($children as $child) {
            $return .= $this->relationshipFunction(
                $child->parent_relationship,
                $this->childRelationship($child),
                $this->docBlock( $this->relationDocBlock($child->relationship_type, 'child'))
            );
        }

        foreach($parents as $parent) {
            $return .= $this->relationshipFunction(
                $parent->child_relationship,
                $this->parentRelationship($parent),
                $this->docBlock( $this->relationDocBlock($parent->relationship_type, 'parent'))
            );
        }
        return $return;
    }

    private function relationDocBlock($relation, $type) {

        $relationship = '@return \\Illuminate\\Database\\Eloquent\\Relations\\';

        switch ($relation) {
            case 'oneToOne':
                $relationship .= $type == 'parent' ? 'BelongsTo' : 'HasOne';
                break;
            case 'oneToMany':
                $relationship .= $type == 'parent' ? 'BelongsTo' : 'HasMany';
                break;
            case 'manyToMany':
                $relationship .= 'BelongsToMany';
        }

        return $relationship;
    }

    /**
     * @param $contents
     * @return string
     */
    private function docBlock($contents) {
        $block = "\n" . '/**';

        if(! is_array($contents)) {
            $contents = [$contents];
        }

        foreach($contents as $line)  {
            $block .= "\n" . ' * ' . $line;
        }

        $block .= "\n" . '*/';

        return $block;
    }

    private function relationshipFunction($name, $relationship, $docBlock = false)
    {
        $return = "\n";
        if($docBlock) {
            $return .= "\n\t" . $docBlock;
        }
        $return .= "\n\t" . 'public function ' . $name . '() ' . "\n\t" . '{';
        $return .= "\n\t\t" . 'return ' . $relationship;
        $return .= "\n\t" . '}';
        return $return;
    }

    private function childRelationship($relationship) {
        $return = '$this->';

        switch ($relationship->relationship_type) {
            case 'oneToOne':
                $return .= 'hasOne(';
                $return .= $relationship->child->full_name . '::class(), ';
                $return .= '\'' . $relationship->parent_foreign_key . '\', \'' . $relationship->child_foreign_key . '\'';
                $return .= ');';
                break;
            case 'oneToMany':
                $return .= $this->hasMany($relationship->child->name, $relationship->parent_foreign_key, $relationship->child_foreign_key);
                break;
            case 'manyToMany:':
                break;
        }

        return $return;
    }

    private function parentRelationship($relationship) {
        $return = '$this->';

        switch ($relationship->relationship_type) {
            case 'oneToOne':
            case 'oneToMany':
            $return .= $this->belongsTo($relationship->parent->full_name, $relationship->parent_foreign_key, $relationship->child_foreign_key);
                break;
            case 'ManyToMany':
                $return .= $this->belongsToMany($relationship->parent->full_name, $relationship->parent->name, $relationship->child->name, $relationship->parent_foreign_key, $relationship->child_foreign_key);
                break;
        }

        return $return;
    }

    private function belongsTo($class, $parent_key, $child_key) {
        $return = 'belongsTo(';
        $return .= $class . '::class(), ';
        $return .= '\'' . $child_key . '\', \'' . $parent_key . '\'';
        $return .= ');';

        return $return;
    }

    private function hasMany($class, $parent_key, $child_key) {
        $return = 'hasMany(';
        $return .= $class . '::class(), ';
        $return .= '\'' . $parent_key . '\', \'' . $child_key . '\'';
        $return .= ');';

        return $return;
    }

    private function belongsToMany($class, $primary_class, $foreign_class, $parent_key, $child_key): string
    {
        $return = 'belongsTo(';
        $return .= $class . '::class(), ';
        $return .= $this->createPivotTableName($foreign_class, $primary_class) . '\'' . $child_key . '\', \'' . $parent_key . '\'';
        $return .= ');';

        return $return;
    }
}
