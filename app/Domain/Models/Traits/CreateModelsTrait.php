<?php


namespace App\Domain\Models\Traits;


use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Models\ModelCreator;

trait CreateModelsTrait
{
    /**
     * @param $models
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function createModels($models, $base_path, $quiet){

        $creator = new ModelCreator;

        foreach($models as $model)
        {
            $creator->create($model, $base_path);
            if(!$quiet) print("\n" . 'Model created for ' . $model->name);
        }
    }
}
