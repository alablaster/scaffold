<?php


namespace App\Contracts;


interface ScaffoldsApplication
{
    public function createDatabase($models, $base_path, $quiet);
    public function createModels($models, $base_path, $quiet);
    public function createResources($models, $base_path, $quiet);
}
