<?php


namespace App\Domain\Core\Traits;


use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait CreateFilesTrait
{
    protected $files;

    public function __construct()
    {
        $this->files = new Filesystem;
    }

    public function storeFile($path, $contents)
    {
        Storage::put(
            $path,
            $contents
        );
    }

    public function getPath($type, $model, $base_path, $option = null)
    {
        switch ($type) {
            case 'controller':
                $path = $this->controllerPath($model->attributes['resource']['name'], $model->namespace, $base_path);
                break;

            case 'request':
                $path = $this->requestPath($model, $base_path, $option);
                return $path;
            default:
                $path = '';
        }

//        $this->files->makeDirectory(dirname($path), 0777, true, true);

        return $path;
    }

//    protected function makeDirectory($path)
//    {
//        if (! $this->files->isDirectory(dirname($path))) {
//            $this->files->makeDirectory(dirname($path), 0777, true, true);
//        }
//
//        return $path;
//    }

    private function controllerPath($name, $namespace, $path)
    {
        $path .= config('scaffold.base_path.controllers');
        $path = $this->attachNamespace($path, $namespace);
        $path .= "/$name" . "Controller.php";
//        $this->files->makeDirectory(dirname($path), 0777, true, true);
        return $path;
    }

    private function requestPath($model, $type, $path)
    {
        $path .= config('scaffold.base_path.requests');
        $path = $this->attachNamespace($path, $model->namespace);
        $path .= "/$model->name";
        $path .= "/$type" . '.php';
//        $this->files->makeDirectory(dirname($path), 0777, true, true);
        return $path;
    }

    private function attachNamespace($path, $namespace = [])
    {
        if (is_array($namespace)) {
            foreach ($namespace as $directory) {
                $path .= "/$directory";
            }
        }
        return $path;
    }
}
