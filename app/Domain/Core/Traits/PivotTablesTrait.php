<?php


namespace App\Domain\Core\Traits;


use Illuminate\Support\Str;

trait PivotTablesTrait
{
    public function createPivotTableName($class_a, $class_b)
    {
        $tables = [
            Str::snake($class_a),
            Str::snake($class_b)
        ];

        sort($tables);

        return $tables[0] . '_' . $tables[1];
    }

    private function sortNames($class_a, $class_b) {
        $tables = [
            Str::snake($class_a),
            Str::snake($class_b)
        ];

        sort($tables);

        return $tables;
    }
}
