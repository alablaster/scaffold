<?php

namespace App\Domain\Projects\Entities;

use App\Domain\Models\Entities\ScaffoldModel;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
            'name',
            'attributes'
        ];

    public function models() {
        return $this->hasMany(ScaffoldModel::class);
    }
}
