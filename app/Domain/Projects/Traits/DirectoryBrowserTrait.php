<?php


namespace App\Domain\Projects\Traits;


use Illuminate\Support\Facades\Storage;

trait DirectoryBrowserTrait
{
    public function getDirectoryAsArray($directory)
    {
        $file_list = Storage::allFiles($directory);

        return $this->makeArray($file_list, $directory);
    }

    private function makeArray(array $file_list, string $directory)
    {
        $return = array();

        foreach($file_list as $file)
        {
            $return = $this->addFileToArray($file, $return, $directory);
        }

        return $return;
    }

    private function addFileToArray(string $file, array $return, $directory)
    {

        $trimmedFile = $this->trimDirectory($file, $directory);

        $file_parts = explode('/', $trimmedFile);

        $reference = '$return';

        foreach($file_parts as $key => $part)
        {
            if($part != null) {
                $reference = $this->addLayer($part, $reference);
            }
        }

        $reference .= " =  \"$file\";";

        eval($reference);

        return $return;
    }

    private function addLayer(string $part, string $return)
    {
        return $return . '["' . $part . '"]';
    }

    private function trimDirectory(string $file, string $directory)
    {
        return str_replace($directory, '', '/' . $file);
    }
}
