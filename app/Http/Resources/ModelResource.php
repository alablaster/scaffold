<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->resource->id,
            'project_id' => $this->resource->project_id,
            'name' => $this->resource->name,
            'fields' => FieldResource::collection($this->whenLoaded('fields')),
            'attributes' => $this->resource->attributes,
            'parents' => $this->resource->parents,
            'children' => $this->resource->children
        ];
    }
}
