<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FieldResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'label' => $this->resource->label,
            'name' => $this->resource->name,
            'type' => $this->resource->type,
            'nullable' => $this->resource->nullable,
            'mass_assignable' => $this->resource->mass_assignable,
            'tags' => $this->resource->attributes
        ];
    }
}
