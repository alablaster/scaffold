<?php

namespace App\Http\Controllers;

use App\Domain\Models\Entities\Field;
use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Projects\Entities\Project;
use App\Http\Requests\Model\Create;
use App\Http\Requests\Model\Patch;
use App\Http\Resources\ModelResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use mysql_xdevapi\Exception;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $fieldTypes = config('scaffold.field_types');
        return view('models.create', compact('fieldTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Create $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Project $project, Create $request)
    {
        $model = $request->validated();
        $model['project_id'] = $project->id;
        $model = ScaffoldModel::create($model);

        $this->createDefaultFields($model);

        if($request->expectsJson()) {
            return response()->json([
                'message' => 'Model successfully created',
                'data' => new ModelResource($model->load('fields')),
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Patch $patch
     * @param ScaffoldModel $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Patch $patch, Project $project, ScaffoldModel $model)
    {

        $data = $patch->validated();

        if($patch->has('attributes')) {
            $attributes = $model->attributes;
            foreach($data['attributes'] as $key => $attribute)
            {
                $attributes[$key] = $attribute;
            }

            $model->attributes = $attributes;
            unset($data['attributes']);
        }

        $model->update($data);

        $model->save();

        return response()->json([
            'message' => 'Model successfully updated',
            'data' => $model,
        ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param ScaffoldModelModel
     * @return \Illuminate\Http\JsonResponse|string[]
     */
    public function destroy(Project $project, ScaffoldModel $model)
    {
        $model->delete();
        return response()->json([
            'message' => 'Model has been deleted'
        ], 204);
    }

    private function createDefaultFields($model) {
        $defaultFields = [];
        $defaultFields[] = [
            'label' => 'ID',
            'name' => 'id',
            'type' => 'incrementing',
            'attributes' => ['Primary Key']
        ];

        $defaultFields[] = [
            'label' => 'Created At',
            'name' => 'created_at',
            'type' => 'datetime',
        ];

        $defaultFields[] = [
            'label' => 'Updated At',
            'name' => 'updated_at',
            'type' => 'datetime',
        ];

        if($model->softDeletable) {
            $generatedFields['deleted_at'] = [
                'label' => 'Deleted At',
                'name' => 'deleted_at',
                'type' => 'datetime',
            ];
        }

        $model->fields()->createMany($defaultFields);
    }
}
