<?php

namespace App\Http\Controllers;

use App\Domain\Models\Entities\ScaffoldModel;
use App\Domain\Projects\Entities\Project;
use App\Http\Resources\ModelResource;
use Illuminate\Http\Request;

class UMLBuilderController extends Controller
{
    public function __invoke(Project $project)
    {

        $models = $project->models->sortBy('name');

        $models->loadMissing('fields', 'children', 'parents');


        return view('UMLBuilder.index')->with([

            'models' => ModelResource::collection($models),
            'project_id' => $project->id
        ]);
    }
}
