<?php

namespace App\Http\Controllers;

use App\Domain\Projects\Entities\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Domain\Projects\Traits\DirectoryBrowserTrait;

class ProjectController extends Controller
{
    use DirectoryBrowserTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::all();
        return view('projects.index', compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255'
        ]);

        $project = Project::create($validated);

        return redirect()->route('umlbuilder', $project->id);

    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Project $project)
    {
        $files = $this->getDirectoryAsArray('staging/project_' . $project->id . '/');

        return view('projects.show', compact('project', 'files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function generate(Project $project)
    {
        Artisan::call("scaffold:laravel", ['project_id' => $project->id, '--silent' => true]);

        return response()->json([
            'message' => "Project has been scaffolded!",
            'data' => $this->getDirectoryAsArray('staging/project_' . $project->id . '/')
        ], 200);
    }
}
