<?php

namespace App\Http\Controllers;

use App\Domain\Models\Entities\Relationship;
use App\Http\Requests\Relationship\create;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RelationshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return array
     */
    public function store(create $request)
    {
        $relationship = Relationship::create($request->validated());

        $this->addRelationshipFields($relationship);

        if($request->expectsJson()) {
            return [
                'data' => $relationship,
                'message' => 'Relationship has been created.'
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    private function addRelationshipFields($relationship)
    {
        $this->addField(
            $relationship->child,
            $this->createForeignKeyField(
                $relationship->child_foreign_key
            ));

        $this->addField(
            $relationship->parent,
            $this->createForeignKeyField(
                $relationship->parent_foreign_key
            ));
    }

    private function addField($model, array $foreign_key_field)
    {
        if($model->fields->where('name', $foreign_key_field['name'])){
            $model->attributes = array_merge($model->attributes, $foreign_key_field['attributes']);
            $model->save();
        } else {
            $model->fields->create($foreign_key_field);
        }
    }

    private function createForeignKeyField($foreign_key)
    {
        return [
            'label' => $foreign_key,
            'name' => $foreign_key,
            'type' => 'bigInteger',
            'attributes' => ['Foreign Key', 'Unsigned']
        ];
    }




}
