<?php

namespace App\Http\Requests\Relationship;

use Illuminate\Foundation\Http\FormRequest;

class create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'relationship_type' => [
                'required',
                'string'
            ],
            'parent_model_id' => [
                'required',
                'exists:scaffold_models,id'
            ],
            'parent_relationship' => [
                'required',
                'string',
                'max:255'
            ],
            'parent_foreign_key' => [
                'required',
                'string',
                'max:255'
            ],
            'child_model_id' => [
                'required',
                'exists:scaffold_models,id'
            ],
            'child_relationship' => [
                'required',
                'string',
                'max:255'
            ],
            'child_foreign_key' => [
                'required',
                'string',
                'max:255'
            ]
        ];
    }
}
