<?php

namespace App\Http\Requests\Model;

use Illuminate\Foundation\Http\FormRequest;

class Patch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'sometimes',
                'string',
                'max:255',
                'unique:scaffold_model,name'
            ],
            'attributes' => [
                'sometimes',
                'array'
            ],
            'softDeletable' => [
                'sometimes',
                'boolean'
            ],
            'timestamps' => [
                'sometimes',
                'boolean'
            ],
            'route' => [
                'sometimes',
                'string',
                'max:255'
            ]
        ];
    }
}
